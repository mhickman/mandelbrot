# mandelbrot

Build Status - ![built status](https://circleci.com/gh/mhickman/mandelbrot/tree/master.png?circle-token=:circle-token)

A program to visualize and explore the [Mandelbrot Set](https://en.wikipedia.org/wiki/Mandelbrot_set) 
and also the [Filled Julia Sets](https://en.wikipedia.org/wiki/Filled_Julia_set) associated with points
on the Mandelbrot Set.
