package io.mhickman;

import java.math.BigDecimal;

import org.junit.Test;

public class RenderParametersTest {

    private static final ComplexBigDecimal CENTER = ComplexBigDecimal.from(BigDecimal.ONE,
            BigDecimal.TEN);

    private static final BigDecimal HEIGHT = BigDecimal.ONE;
    private static final BigDecimal WIDTH = BigDecimal.TEN;

    @Test(expected = IllegalArgumentException.class)
    public void negativeHeightThrowsException() {
        RenderParameters.builder()
                .setCenter(CENTER)
                .setWidth(WIDTH)
                .setHeight(HEIGHT)
                .setnHeight(-10)
                .setnWidth(5)
                .createRenderParameters();
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeWidthThrowsException() {
        RenderParameters.builder()
                .setCenter(CENTER)
                .setWidth(WIDTH)
                .setHeight(HEIGHT)
                .setnHeight(10)
                .setnWidth(-5)
                .createRenderParameters();
    }

    @Test(expected = NullPointerException.class)
    public void nullCenterThrowsException() {
        RenderParameters.builder()
                .setWidth(WIDTH)
                .setHeight(HEIGHT)
                .setnHeight(10)
                .setnWidth(5)
                .createRenderParameters();
    }

    @Test(expected = NullPointerException.class)
    public void nullWidthThrowsException() {
        RenderParameters.builder()
                .setCenter(CENTER)
                .setHeight(HEIGHT)
                .setnHeight(10)
                .setnWidth(5)
                .createRenderParameters();
    }

    @Test(expected = NullPointerException.class)
    public void nullHeightThrowsException() {
        RenderParameters.builder()
                .setCenter(CENTER)
                .setWidth(WIDTH)
                .setnHeight(10)
                .setnWidth(5)
                .createRenderParameters();
    }

}
