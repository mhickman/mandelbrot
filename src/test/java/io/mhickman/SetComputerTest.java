package io.mhickman;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class SetComputerTest {

    private final SetComputer setComputer;

    public SetComputerTest(SetComputer setComputer) {
        this.setComputer = setComputer;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getSetComputersToTest() {
        Object[] doubleSetComputer = {DoubleSetComputer.withMaxIterations(MAX_ITERATIONS)};
        Object[] bigDecimalSetComputer = {BigDecimalSetComputer.withMaxIterations(MAX_ITERATIONS)};

        return Lists.newArrayList(doubleSetComputer, bigDecimalSetComputer);
    }

    private static final ImmutableList<ComplexBigDecimal> NUMBERS_NOT_IN_MANDELBROT = ImmutableList.<ComplexBigDecimal>builder()
            .add(ComplexBigDecimal.fromDoubles(.5, 0))
            .add(ComplexBigDecimal.ONE)
            .add(ComplexBigDecimal.fromDoubles(-1, .5))
            .build();

    private static final ImmutableList<ComplexBigDecimal> NUMBERS_IN_MANDELBROT = ImmutableList.<ComplexBigDecimal>builder()
            .add(ComplexBigDecimal.ZERO)
            .add(ComplexBigDecimal.fromDoubles(.125, 0)) // 1/8th=
            .add(ComplexBigDecimal.ONE.negate())
            .add(ComplexBigDecimal.fromDoubles(.125, .5))
            .add(ComplexBigDecimal.fromDoubles(-1, .125)) // -1 + i/8
            .build();

    private static final int MAX_ITERATIONS = 1000;

    @Test
    public void notInMandelbrotSet() {
        for (ComplexBigDecimal c : NUMBERS_NOT_IN_MANDELBROT) {
            assertThat(computeForSinglePoint(c).isInMadelbrotSet()).isFalse();
        }
    }

    @Test
    public void inMandelbrotSet() {
        for (ComplexBigDecimal c : NUMBERS_IN_MANDELBROT) {
            assertThat(computeForSinglePoint(c).isInMadelbrotSet()).isTrue();
        }
    }

    private MandelbrotPoint computeForSinglePoint(ComplexBigDecimal c) {
        return Iterables.getFirst(setComputer.computeSet(generateParamsForNumber(c)), null);
    }

    private static RenderParameters generateParamsForNumber(ComplexBigDecimal c) {
        return RenderParameters.builder()
                .setCenter(c)
                .setHeight(BigDecimal.ZERO)
                .setWidth(BigDecimal.ZERO)
                .setnHeight(1)
                .setnWidth(1)
                .setPrecision(100)
                .createRenderParameters();
    }
}
