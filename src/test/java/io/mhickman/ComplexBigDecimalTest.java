package io.mhickman;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.math.MathContext;

import org.junit.Test;

public class ComplexBigDecimalTest {

    private static final BigDecimal ONE = BigDecimal.ONE;
    private static final BigDecimal TWO = BigDecimal.valueOf(2L);
    private static final BigDecimal THREE = BigDecimal.valueOf(3L);
    private static final MathContext MATH_CONTEXT = new MathContext(10);

    @Test
    public void complexNumbersAdd() {
        ComplexBigDecimal first = ComplexBigDecimal.from(ONE, ONE);
        ComplexBigDecimal second = ComplexBigDecimal.from(TWO, ONE);
        ComplexBigDecimal expected = ComplexBigDecimal.from(ONE.add(TWO), ONE.add(ONE));

        assertThat(first.add(second, MATH_CONTEXT)).isEqualTo(expected);
    }

    @Test
    public void complexNumbersMultiply() {
        ComplexBigDecimal first = ComplexBigDecimal.from(ONE, ONE);
        ComplexBigDecimal second = ComplexBigDecimal.from(TWO, ONE);
        ComplexBigDecimal expected = ComplexBigDecimal.from(ONE, THREE);

        assertThat(first.multiply(second, MATH_CONTEXT)).isEqualTo(expected);
    }

    @Test
    public void complexNumberMagnitude() {
        ComplexBigDecimal testNumber = ComplexBigDecimal.from(TWO, THREE);
        assertThat(testNumber.mag2(MATH_CONTEXT)).isEqualTo(BigDecimal.valueOf(13L));
    }

}
