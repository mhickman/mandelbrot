package io.mhickman;

import java.awt.*;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class ColorPalette {

    private final Color firstColor;
    private final Color secondColor;
    private final int maxIterations;
    private final int minIterations;

    private ColorPalette(Color firstColor, Color secondColor, int maxIterations, int minIterations) {
        checkNotNull(firstColor);
        checkNotNull(secondColor);
        checkArgument(maxIterations > 0);
        checkArgument(minIterations >= 0);

        this.firstColor = firstColor;
        this.secondColor = secondColor;
        this.maxIterations = maxIterations;
        this.minIterations = minIterations;
    }

    public static ColorPalette fromMadelbrot(Color firstColor, Color secondColor, List<MandelbrotPoint> points) {
        MaxMin maxMin = findMaxMinIterations(points);
        return new ColorPalette(firstColor, secondColor, (int) maxMin.maxIterations, (int) maxMin.minIterations);
    }

    public Color getColor(int iterations) {
        double percent = percent(iterations);

        int alpha = interpolate(percent, firstColor.getAlpha(), secondColor.getAlpha());
        int red = interpolate(percent, firstColor.getRed(), secondColor.getRed());
        int green = interpolate(percent, firstColor.getGreen(), secondColor.getGreen());
        int blue = interpolate(percent, firstColor.getBlue(), secondColor.getBlue());

        return new Color(red, green, blue, alpha);
    }

    private static MaxMin findMaxMinIterations(Iterable<MandelbrotPoint> points) {
        long max = 0;
        long min = Long.MAX_VALUE;

        for (MandelbrotPoint point : points) {
            if (point.getTimeToEscape() > max) {
                max = point.getTimeToEscape();
            }

            if (point.getTimeToEscape() < min && point.getTimeToEscape() >= 0) {
                min = point.getTimeToEscape();
            }
        }

        return new MaxMin(max, min);
    }

    private static int interpolate(double percent, int a, int b) {
        return Math.min(
                (int) Math.round(percent*a + (1-percent)*b),
                255);
    }

    private double percent(int iterations) {
        return 1.0*(iterations - minIterations)/(maxIterations - minIterations);
    }

    private static class MaxMin {
        final long maxIterations;
        final long minIterations;

        MaxMin(long maxIterations, long minIterations) {
            this.maxIterations = maxIterations;
            this.minIterations = minIterations;
        }
    }

}
