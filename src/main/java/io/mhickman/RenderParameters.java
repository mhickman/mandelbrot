package io.mhickman;


import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.math.MathContext;

import com.google.common.base.Objects;

public class RenderParameters {

    private final ComplexBigDecimal center;
    private final BigDecimal width;
    private final BigDecimal height;
    private final int nHeight;
    private final int nWidth;
    private final MathContext mathContext;

    private RenderParameters(ComplexBigDecimal center,
                             BigDecimal width,
                             BigDecimal height,
                             int nHeight,
                             int nWidth,
                             int precision) {
        checkNotNull(center);
        checkNotNull(width);
        checkNotNull(height);
        checkArgument(nHeight > 0);
        checkArgument(nWidth > 0);
        checkArgument(precision > 0);

        this.center = center;
        this.width = width;
        this.height = height;
        this.nHeight = nHeight;
        this.nWidth = nWidth;
        this.mathContext = new MathContext(precision);
    }

    public ComplexBigDecimal getCenter() {
        return center;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public int getnHeight() {
        return nHeight;
    }

    public int getnWidth() {
        return nWidth;
    }

    public MathContext getMathContext() {
        return mathContext;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RenderParameters that = (RenderParameters) o;
        return nHeight == that.nHeight &&
                nWidth == that.nWidth &&
                Objects.equal(center, that.center) &&
                Objects.equal(width, that.width) &&
                Objects.equal(height, that.height);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(center, width, height, nHeight, nWidth);
    }

    public static class Builder {
        private ComplexBigDecimal center;
        private BigDecimal width;
        private BigDecimal height;
        private int nHeight;
        private int nWidth;
        private int precision = 20; // default

        private Builder() {

        }

        public Builder setCenter(ComplexBigDecimal center) {
            this.center = center;
            return this;
        }

        public Builder setWidth(BigDecimal width) {
            this.width = width;
            return this;
        }

        public Builder setWidth(double width) {
            this.width = BigDecimal.valueOf(width);
            return this;
        }

        public Builder setHeight(BigDecimal height) {
            this.height = height;
            return this;
        }

        public Builder setnHeight(int nHeight) {
            this.nHeight = nHeight;
            return this;
        }

        public Builder setnWidth(int nWidth) {
            this.nWidth = nWidth;
            return this;
        }

        public Builder setPrecision(int precision) {
            this.precision = precision;
            return this;
        }

        public RenderParameters createRenderParameters() {
            return new RenderParameters(center, width, height, nHeight, nWidth, precision);
        }
    }

}
