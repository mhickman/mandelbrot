package io.mhickman;


import static com.google.common.base.Preconditions.checkArgument;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

public final class BigDecimalSetComputer implements SetComputer {

    private static final BigDecimal FOUR = BigDecimal.valueOf(4L);
    private static final BigDecimal TWO = BigDecimal.valueOf(2L);

    private final int maxIterations;
    private final ListeningExecutorService executorService;

    private BigDecimalSetComputer(int maxIterations) {
        checkArgument(maxIterations > 0);

        this.maxIterations = maxIterations;
        this.executorService = ExecutorHelper.newFixedThreadPoolExecutor(8);
    }

    public static SetComputer withMaxIterations(int maxIterations) {
        return new BigDecimalSetComputer(maxIterations);
    }

    private static Function<ImagePosition, MandelbrotPoint> mandelbrotFunction(final int maxIterations,
                                                                               final MathContext mc) {
        return (imagePosition) -> {
            ComplexBigDecimal current = ComplexBigDecimal.ZERO;
            int iterations = 0;

            while (iterations <= maxIterations) {
                if (current.mag2(mc).compareTo(FOUR) > 0) { // we've escaped
                    return new MandelbrotPoint(imagePosition, iterations);
                } else {
                    current = current.pow2(mc).add(imagePosition.getValue(), mc);
                    iterations++;
                }
            }

            return new MandelbrotPoint(imagePosition, -1); // looks like it probably IS in the set.
        };
    }

    private static ComplexBigDecimal getTopLeft(RenderParameters parameters) {
        return parameters.getCenter().subtract(
                ComplexBigDecimal.fromReal(parameters.getWidth().divide(TWO,
                        parameters.getMathContext())), parameters.getMathContext())
                .add(ComplexBigDecimal.fromImaginary(parameters.getHeight().divide(TWO,
                        parameters.getMathContext())), parameters.getMathContext());
    }

    private static List<ImagePosition> generateImagePositionGrid(ComplexBigDecimal topLeft,
                                                                 int nX,
                                                                 int nY,
                                                                 BigDecimal deltaX,
                                                                 BigDecimal deltaY,
                                                                 MathContext mc) {
        ImmutableList.Builder<ImagePosition> positions = ImmutableList.builder();

        for (long x = 0; x < nX; x++) {
            for (long y = 0; y < nY; y++) {
                ComplexBigDecimal next = topLeft
                        .add(ComplexBigDecimal.fromReal(deltaX.multiply(BigDecimal.valueOf(x))), mc)
                        .add(ComplexBigDecimal.fromImaginary(deltaY.multiply(BigDecimal.valueOf(y))), mc);

                positions.add(new ImagePosition((int) x, (int) y, next));
            }
        }

        return positions.build();
    }

    @Override
    public List<MandelbrotPoint> computeSet(RenderParameters parameters) {
        final BigDecimal deltaY = parameters
                .getHeight()
                .negate()
                .divide(BigDecimal.valueOf(parameters.getnHeight()), parameters.getMathContext());

        final BigDecimal deltaX = parameters
                .getWidth()
                .divide(BigDecimal.valueOf(parameters.getnWidth()), parameters.getMathContext());

        List<ImagePosition> positionGrid = generateImagePositionGrid(getTopLeft(parameters),
                parameters.getnWidth(),
                parameters.getnHeight(),
                deltaX,
                deltaY,
                parameters.getMathContext());

        final Function<ImagePosition, MandelbrotPoint> localFunction = mandelbrotFunction(maxIterations,
                parameters.getMathContext());

        List<ListenableFuture<MandelbrotPoint>> futures = Lists.newArrayListWithCapacity(positionGrid.size());

        final Ticker ticker = new Ticker(positionGrid.size(), 100);

        for (ImagePosition imagePosition : positionGrid) {
            futures.add(executorService.submit(() -> {
                MandelbrotPoint toReturn = localFunction.apply(imagePosition);
                ticker.tick();
                return toReturn;
            }));
        }

        try {
            return Futures.allAsList(futures).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null; // we fail.
    }

    private static final class Ticker {

        private final int total;
        private final int printEvery;
        private final AtomicInteger counter;

        private Ticker(int total, int printEvery) {
            this.total = total;
            this.printEvery = printEvery;
            this.counter = new AtomicInteger(0);
        }

        private void tick() {
            int next = counter.incrementAndGet();

            if ((next % printEvery) == 0) {
                System.out.println("Done with " + next + "/" + total + ". " + 100.0 * next / total + "% done.");
            }
        }

    }

}
