package io.mhickman;

import com.google.common.base.Objects;

public class ImagePosition {
    private final int xPosition;
    private final int yPosition;
    private final ComplexBigDecimal value;

    public ImagePosition(int xPosition, int yPosition, ComplexBigDecimal value) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.value = value;
    }

    public int getxPosition() {
        return xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public ComplexBigDecimal getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImagePosition that = (ImagePosition) o;
        return xPosition == that.xPosition &&
                yPosition == that.yPosition &&
                Objects.equal(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(xPosition, yPosition, value);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("xPosition", xPosition)
                .add("yPosition", yPosition)
                .add("value", value)
                .toString();
    }
}
