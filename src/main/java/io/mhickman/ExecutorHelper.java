package io.mhickman;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ExecutorHelper {

    private static final int QUEUE_SIZE = 100000000;

    private ExecutorHelper() {
        // do not instantiate
    }

    public static ListeningExecutorService newFixedThreadPoolExecutor(int numThreads) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(numThreads,
                numThreads,
                1000L,
                TimeUnit.DAYS,
                new ArrayBlockingQueue<>(QUEUE_SIZE));

        ExecutorService executorService = MoreExecutors.getExitingExecutorService(threadPoolExecutor);
        return MoreExecutors.listeningDecorator(executorService);
    }

}
