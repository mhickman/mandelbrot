package io.mhickman;

public class MandelbrotPoint {
    private final ImagePosition imagePosition;
    private final long timeToEscape;
    private final boolean inMadelbrotSet;

    private static final long TIME_SENTINEL_VALUE = -1L;

    MandelbrotPoint(ImagePosition imagePosition, long timeToEscape) {
        this.imagePosition = imagePosition;
        this.timeToEscape = timeToEscape;
        this.inMadelbrotSet = timeToEscape == TIME_SENTINEL_VALUE;
    }

    public ImagePosition getImagePosition() {
        return imagePosition;
    }

    public long getTimeToEscape() {
        return timeToEscape;
    }

    public boolean isInMadelbrotSet() {
        return inMadelbrotSet;
    }

}
