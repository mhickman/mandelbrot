package io.mhickman;

import java.math.BigDecimal;
import java.math.MathContext;

import com.google.common.base.Objects;

public class ComplexBigDecimal {

    private final BigDecimal real;
    private final BigDecimal imaginary;

    private static final BigDecimal TWO = BigDecimal.valueOf(2L);

    public static final ComplexBigDecimal ZERO = new ComplexBigDecimal(BigDecimal.ZERO,
            BigDecimal.ZERO);

    public static final ComplexBigDecimal ONE = new ComplexBigDecimal(BigDecimal.ONE,
            BigDecimal.ZERO);

    public static final ComplexBigDecimal I = new ComplexBigDecimal(BigDecimal.ZERO,
            BigDecimal.ONE);

    private ComplexBigDecimal(BigDecimal real, BigDecimal imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public static ComplexBigDecimal from(BigDecimal real, BigDecimal imaginary) {
        return new ComplexBigDecimal(real, imaginary);
    }

    public static ComplexBigDecimal from(long real, long imaginary) {
        return new ComplexBigDecimal(BigDecimal.valueOf(real), BigDecimal.valueOf(imaginary));
    }

    public static ComplexBigDecimal fromReal(BigDecimal real) {
        return new ComplexBigDecimal(real, BigDecimal.ZERO);
    }

    public static ComplexBigDecimal fromImaginary(BigDecimal imaginary) {
        return new ComplexBigDecimal(BigDecimal.ZERO, imaginary);
    }

    public static ComplexBigDecimal fromDoubles(double real, double imaginary) {
        return new ComplexBigDecimal(BigDecimal.valueOf(real), BigDecimal.valueOf(imaginary));
    }

    public ComplexBigDecimal negate() {
        return new ComplexBigDecimal(real.negate(), imaginary.negate());
    }

    public ComplexBigDecimal pow2(MathContext mc) {
        return ComplexBigDecimal.from(
                real.pow(2, mc).subtract(imaginary.pow(2, mc), mc),
                TWO.multiply(real.multiply(imaginary, mc), mc)
        );
    }

    public BigDecimal mag2(MathContext mc) {
        return real.pow(2, mc).add(imaginary.pow(2, mc), mc);
    }

    public BigDecimal getReal() {
        return real;
    }

    public BigDecimal getImaginary() {
        return imaginary;
    }

    public ComplexBigDecimal add(ComplexBigDecimal other, MathContext mc) {
        return new ComplexBigDecimal(real.add(other.real, mc), imaginary.add(other.imaginary, mc));
    }

    public ComplexBigDecimal subtract(ComplexBigDecimal other, MathContext mc) {
        return new ComplexBigDecimal(real.subtract(other.real, mc), imaginary.subtract(other.imaginary, mc));
    }

    public ComplexBigDecimal multiply(ComplexBigDecimal other, MathContext mc) {
        return new ComplexBigDecimal(real.multiply(other.real, mc)
                .subtract(imaginary.multiply(other.imaginary, mc), mc),
                real.multiply(other.imaginary, mc)
                        .add(imaginary.multiply(other.real, mc), mc));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComplexBigDecimal that = (ComplexBigDecimal) o;
        return Objects.equal(real, that.real) &&
                Objects.equal(imaginary, that.imaginary);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(real, imaginary);
    }

    @Override
    public String toString() {
        return "ComplexBigDecimal{" +
                "real=" + real +
                ", imaginary=" + imaginary +
                '}';
    }
}
