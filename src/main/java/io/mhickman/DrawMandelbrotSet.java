package io.mhickman;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class DrawMandelbrotSet {

    private static final int WHITE = Color.WHITE.getRGB();
    private static final int BLACK = Color.BLACK.getRGB();

    public static void main(String[] args) throws InterruptedException, IOException {
        BufferedImage bufferedImage = new BufferedImage(2000, 2000, BufferedImage.TYPE_INT_ARGB);
        SetComputer setComputer = DoubleSetComputer.withMaxIterations(50);

        RenderParameters renderParameters = RenderParameters.builder()
                .setCenter(ComplexBigDecimal.fromDoubles(-.6, 0.0))
                .setWidth(BigDecimal.valueOf(3L))
                .setHeight(BigDecimal.valueOf(3L))
                .setnHeight(2000)
                .setnWidth(2000)
                .createRenderParameters();

        List<MandelbrotPoint> points = setComputer.computeSet(renderParameters);
        ColorPalette colorPalette = ColorPalette.fromMadelbrot(Color.PINK, Color.BLACK, points);

        for (MandelbrotPoint point : points) {
            int rgb;
            if (point.isInMadelbrotSet()) {
                rgb = BLACK;
            } else {
                rgb = colorPalette.getColor((int) point.getTimeToEscape()).getRGB();
            }
            bufferedImage.setRGB(point.getImagePosition().getxPosition(),
                    point.getImagePosition().getyPosition(),
                    rgb);

        }

        Image smoothImage = bufferedImage.getScaledInstance(1000, 1000, Image.SCALE_SMOOTH);
        BufferedImage bufferedSmoothImage = new BufferedImage(1000, 1000, BufferedImage.TYPE_INT_ARGB);
        bufferedSmoothImage.getGraphics().drawImage(smoothImage, 0, 0, null);

        JFrame jFrame = new JFrame();
        jFrame.getContentPane().setLayout(new FlowLayout());
        jFrame.add(new JLabel(new ImageIcon(smoothImage)));
        jFrame.pack();
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setVisible(true);

        File file = new File("/Users/matt/Desktop/newImage.png");
        ImageIO.write(bufferedSmoothImage, "png", file);
    }

}
