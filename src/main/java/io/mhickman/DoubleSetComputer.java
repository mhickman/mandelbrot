package io.mhickman;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.stream.Collectors;

public class DoubleSetComputer implements SetComputer {

    private static final double ESCAPE_DISTANCE = 4.0;
    private final int maxIterations;

    private DoubleSetComputer(int maxIterations) {
        this.maxIterations = maxIterations;
    }

    public static DoubleSetComputer withMaxIterations(int maxIterations) {
        return new DoubleSetComputer(maxIterations);
    }

    private static ImagePosition getImagePosition(DoubleImagePosition doubleImagePosition) {
        return new ImagePosition(doubleImagePosition.nx, doubleImagePosition.ny,
                ComplexBigDecimal.fromDoubles(doubleImagePosition.location.x, doubleImagePosition.location.y));
    }

    private static List<DoubleImagePosition> generateGrid(DoublePoint topLeft,
                                                          double height, double width, int nX, int nY) {
        ImmutableList.Builder<DoubleImagePosition> grid = ImmutableList.builder();

        double dX = width / nX;
        double dY = -1.0 * height / nY;

        for (int x = 0; x < nX; x++) {
            for (int y = 0; y < nY; y++) {
                grid.add(new DoubleImagePosition(new DoublePoint(topLeft.x + x * dX, topLeft.y + y * dY),
                        x, y));
            }
        }

        return grid.build();
    }

    private static DoublePoint getTopLeftCorner(DoublePoint center, double height, double width) {
        double x = center.x - width / 2;
        double y = center.y + height / 2;

        return new DoublePoint(x, y);
    }

    @Override
    public List<MandelbrotPoint> computeSet(RenderParameters parameters) {
        DoublePoint center = new DoublePoint(parameters.getCenter().getReal().doubleValue(),
                parameters.getCenter().getImaginary().doubleValue());

        double width = parameters.getWidth().doubleValue();
        double height = parameters.getHeight().doubleValue();

        List<DoubleImagePosition> grid = generateGrid(getTopLeftCorner(center, height, width),
                height, width, parameters.getnWidth(), parameters.getnHeight());

        return grid.parallelStream().map(doublePosition -> {
            double x = 0.0;
            double y = 0.0;
            int iteration = 0;

            while (iteration < maxIterations) {
                if ((x * x + y * y) > ESCAPE_DISTANCE) {
                    return new MandelbrotPoint(getImagePosition(doublePosition), iteration);
                } else {
                    double newX = x * x - y * y + doublePosition.location.x;
                    double newY = 2.0 * x * y + doublePosition.location.y;
                    x = newX;
                    y = newY;
                    iteration++;
                }
            }

            return new MandelbrotPoint(getImagePosition(doublePosition), -1);
        }).collect(Collectors.toList());
    }

    private static class DoubleImagePosition {
        final DoublePoint location;
        final int nx, ny;

        private DoubleImagePosition(DoublePoint location, int nx, int ny) {
            this.location = location;
            this.nx = nx;
            this.ny = ny;
        }

        @Override
        public String toString() {
            return "DoubleImagePosition{" +
                    "location=" + location +
                    ", nx=" + nx +
                    ", ny=" + ny +
                    '}';
        }
    }

    private static class DoublePoint {

        final double x, y;

        private DoublePoint(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "DoublePoint{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

}
