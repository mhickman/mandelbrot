package io.mhickman;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.google.common.util.concurrent.ListenableFuture;

public interface SetComputer {
    List<MandelbrotPoint> computeSet(RenderParameters parameters);
}
