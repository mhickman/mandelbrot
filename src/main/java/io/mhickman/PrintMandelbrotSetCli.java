package io.mhickman;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Prints a basic pic of the Mandelbrot set.
 */
public class PrintMandelbrotSetCli {

    // should give a reasonable view of the Mandelbrot Set.
    private static final RenderParameters TEST_PARAMS = RenderParameters.builder()
            .setCenter(ComplexBigDecimal.ZERO)
            .setWidth(BigDecimal.valueOf(4L))
            .setHeight(BigDecimal.valueOf(2L))
            .setnHeight(500)
            .setnWidth(1000)
            .setPrecision(14)
            .createRenderParameters();

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        SetComputer setComputer = DoubleSetComputer.withMaxIterations(100);
        List<MandelbrotPoint> points = setComputer.computeSet(TEST_PARAMS);

        boolean[][] setPicture = new boolean[TEST_PARAMS.getnHeight()][TEST_PARAMS.getnWidth()];

        for (MandelbrotPoint mp : points) {
            setPicture[mp.getImagePosition().getyPosition()][mp.getImagePosition().getxPosition()] = mp.isInMadelbrotSet();
        }

        for (boolean[] xs : setPicture) {
            for (boolean pixel : xs) {
                if (pixel) {
                    System.out.print('0');
                } else {
                    System.out.print('1');
                }
            }

            System.out.println();
        }
    }

}
